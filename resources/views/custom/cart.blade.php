@extends('layouts.home-layout')

@section('content')
<div class="cart_section">
    <div class="container-fluid">
        @if(session()->has('success'))
            <div class="alert alert-success" role="alert">
                <h3>{{ session()->get('success') }}</h3>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="cart_container">
                    @if(!empty($total_items))
                    <form action="{{ route('checkout') }}" method="post" enctype="multipart/form-data">
                        @php
                            if($total_items == '1'){
                                $string = 'item';
                            }else{
                                $string = 'items';
                            }
                            $sum = 0;
                        @endphp
                    <div class="cart_title">Shopping Cart<small> ({{ $total_items }} {{ $string }} in your cart) </small></div>
                    <div class="cart_items">
                        <ul class="cart_list">
                            @foreach ($cart as $items)
                                @foreach ($items as $item)
                                <li class="cart_item clearfix">
                                    <div class="cart_item_image"><img src="{{ asset('images/').'/'.$item->photo }}" alt=""></div>
                                    <div class="cart_item_info d-flex flex-md-row flex-column justify-content-between">
                                        <div class="cart_item_name cart_info_col">
                                            <div class="cart_item_title">Name</div>
                                            <div class="cart_item_text">{{ $item->title }}</div>
                                        </div>
                                        <div class="cart_item_quantity cart_info_col">
                                            <div class="cart_item_title">Quantity</div>
                                            <div class="cart_item_text">1</div>
                                        </div>
                                        <div class="cart_item_price cart_info_col">
                                            <div class="cart_item_title">Price</div>
                                            <div class="cart_item_text">₹ {{ $item->price }}</div>
                                        </div>
                                        <div class="cart_item_total cart_info_col">
                                            <div class="cart_item_title">Total</div>
                                            <div class="cart_item_text">₹ {{ $item->price }}</div>
                                        </div>
                                        <div class="cart_item_total remove-item cart_info_col">
                                            <div class="cart_item_title">Remove</div>
                                            <div class="cart_item_text remove"><a href="{{ route('remove-item', [ 'id' => $item->id ]) }}"><i class="fas fa-trash-alt"></i></a></div>
                                        </div>
                                        <input type="hidden" name="product_id[]" value="{{ $item->id }}">
                                    </div>
                                </li>
                                {{-- get sum total amount --}}
                                @php
                                    $sum = $sum + $item->price;
                                @endphp
                                @endforeach
                            @endforeach
                        </ul>
                    </div>
                    <div class="order_total">
                        <div class="order_total_content text-md-right">
                            <div class="order_total_title">Order Total:</div>
                            <div class="order_total_amount">₹ {{ $sum }}</div>
                        </div>
                        <div class="text-md-right">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="materialUnchecked" required>
                                <label class="form-check-label" for="materialUnchecked">Cash on Delivery</label>
                            </div>
                        </div>
                    </div>   
                    <div class="cart_buttons"> 
                        <button type="button" class="button cart_button_clear">
                            <a href="{{ route('home') }}">Continue Shopping</a>
                        </button> 
                        <button type="submit" class="button cart_button_checkout">Proceed to Checkout</button> 
                    </div>
                    @csrf
                    </form> 
                    @else
                        <h2>Your shopping cart is empty.</h2> <br/>
                        <div> <button type="button" class="button cart_button_clear"><a href="{{ route('home') }}">Continue Shopping</a></button></div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection