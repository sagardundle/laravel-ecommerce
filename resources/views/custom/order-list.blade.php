<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            @can('admin_permission')
                {{ __('All Orders') }}
            @elsecan('user_permission')                
                {{ __('My Orders') }}
            @endcan
        </h2>
    </x-slot>

    <div class="container" style="margin-top:40px;">
        <div class="table-responsive">
            <table id="orders" class="table">
                @if(!empty($orders))
                    <tr>
                    <th>id</th>
                    <th>title</th>
                    <th>category</th>
                    <th>description</th>
                    <th>photo</th>
                    <th>price</th>
                    <th>name</th>
                    <th>email</th>
                    <th>address</th>
                    </tr>                
                @endif
                @forelse ($orders as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->category }}</td>
                        <td>{{ $item->description }}</td>
                        <td><img class="img-responsive" width="130px"  alt="eCommerce Product List" src="{{ asset('images/').'/'.$item->photo }}"></td>
                        <td>{{ $item->price }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{!! nl2br($item->address) !!}</td>
                    </tr>            
                @empty
                    <h2>No orders</h2>
                @endforelse
            </table>
        </div>
    </div>

</x-app-layout>