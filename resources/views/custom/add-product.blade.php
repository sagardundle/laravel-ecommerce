<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add Product') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if(session()->has('status'))
                        <div class="alert alert-success" role="alert">
                            <h3>{{ session()->get('status') }}</h3>
                        </div>
                    @endif

                    @if ($errors->all())
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form action="{{ route('add-product') }}" method="post" class="mt-6" enctype="multipart/form-data">
                        <label for="title" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Title*</label>
                        <input id="title" type="text" name="title" placeholder="Title" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />

                        <label for="category" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Select category*</label>
                        <select name="category" id="category">
                            <option value="games">Games</option>
                            <option value="movies">Movies</option>
                            <option value="electronics">Electronics</option>
                        </select>

                        <label for="description" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Description*</label>
                        <textarea name="description" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" id="" cols="30" rows="10"></textarea>

                        <label for="title" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Image*</label>
                        <input id="image" type="file" name="photo" placeholder="Image" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />
                        
                        <label for="price" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Price* (INR)</label>
                        <input id="price" type="text" name="price" placeholder="price" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />

                        @csrf
                        <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                            <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Save</button>         
                        </div>               
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>