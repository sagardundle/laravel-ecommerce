<!-- Main Navigation -->
<header>

    <!-- Navbar -->
    <nav class="navbar navbar-expand-md navbar-light fixed-top scrolling-navbar">
      <div class="container-fluid">
  
        <!-- Brand -->
        <a class="navbar-brand" href="{{ route('home') }}">
            <img src="{{ asset('images/logo.svg') }}" style="width: 100%;" alt="" srcset="">
        </a>
  
        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
          aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
  
        <!-- Links -->
        <div class="collapse navbar-collapse" id="basicExampleNav">
  
          <!-- Right -->
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="{{ route('cart') }}" class="nav-link navbar-link-2 waves-effect">
                <i class="fas fa-shopping-cart pl-0"></i>
                @if (session()->has('total_items_in_cart'))                    
                    <span class="badge badge-pill red">{{ session()->get('total_items_in_cart') }}</span>
                @else
                    <span class="badge badge-pill red">0</span>
                @endif
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('games') }}" class="nav-link waves-effect">
                Games
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('movies') }}" class="nav-link waves-effect">
                Movies
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('electronics') }}" class="nav-link waves-effect">
                Electronics
              </a>
            </li>
            @guest
            <li class="nav-item">
              <a href="{{ route('login') }}" class="nav-link waves-effect" style="background-color: #3fff45;">
                Sign in
              </a>
            </li>
            <li class="nav-item pl-2 mb-2 mb-md-0">
              <a href="{{ route('register') }}" class="nav-link waves-effect" style="background-color: #689cf3!important;">Sign up</a>
            </li>
            {{-- <li class="nav-item pl-2 mb-2 mb-md-0">
              <a href="{{ route('register') }}" type="button"
                class="btn btn-outline-info btn-md btn-rounded btn-navbar waves-effect waves-light">Sign
                up</a>
            </li> --}}
            @endguest
            @auth
            <li class="nav-item pl-2 mb-2 mb-md-0">
                <a href="{{ route('dashboard') }}" class="nav-link waves-effec">Dashboard</a>
            </li>
            <li class="nav-item">
              <form method="POST" action="{{ route('logout') }}">
                @csrf
              <a onclick="event.preventDefault(); this.closest('form').submit();" class="nav-link waves-effect">
                logout
              </a>
              </form>
            </li>
            @endauth
          </ul>
  
        </div>
        <!-- Links -->
      </div>
    </nav>
    <!-- Navbar -->
  
    {{-- <div class="jumbotron color-grey-light mt-70">
        <div class="d-flex align-items-center h-100">
            <div class="container text-center py-5">
                <h1 class="mb-3">Welcome to E-Commerce website</h1>
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tincidunt justo non massa
                porta molestie.</p>
            </div>
        </div>
    </div> --}}
      
</header>
<!-- Main Navigation -->