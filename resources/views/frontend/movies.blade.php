@extends('layouts.home-layout')

@section('content')
    <div class="container" style="margin-top:120px; margin-bottom:50px;">
        <h1 class="text-center">All Movies</h1>
        <div class="row">
            @foreach($movies as $item)
            <div class="col-md-4" style="padding:15px;">
                <div style="display:inline-block; border:solid 1px #808080; padding:15px">
                    <div>
                        <img class="img-responsive" alt="eCommerce Product List" src="{{ asset('images/').'/'.$item->photo }}" />
                        <br />
                        <h2 class="pull-right">₹ {{ $item->price }}</h2>
                        <h2><a href="javascript:void(0);">{{ $item->title }}</a></h2>
                        <br />
                        <p class="text-justify">{{ $item->description }}</p>
                    </div>
                    <br />
                    <br />
                    <div class="btn-ground text-center" style="padding-bottom: 30px">
                        <form action="{{ route('add-cart') }}" method="post">
                            <input type="hidden" value="{{ $item->id }}" name="product_id">
                            @csrf
                        <button type="submit" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Add</button>
                        </form>
                        {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#productmodal1"><i class="fa fa-info"></i> Info</button> --}}
                    </div>
                </div>
            </div>                
            @endforeach
        </div>
    </div>
@endsection