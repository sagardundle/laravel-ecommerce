<?php

use App\Http\Controllers\RemoveItem;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\GamesController;
use App\Http\Controllers\MoviesController;
use App\Http\Controllers\AddCartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\AddProductController;
use App\Http\Controllers\GetProductController;
use App\Http\Controllers\OrderModelController;
use App\Http\Controllers\ElectronicsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// homepage front
Route::get('/', [ GetProductController::class, 'get' ])->name('home');

Route::get('/games', [ GamesController::class, 'list' ])->name('games');
Route::get('/movies', [ MoviesController::class, 'list' ])->name('movies');
Route::get('/electronics', [ ElectronicsController::class, 'list' ])->name('electronics');

// for cart
Route::post('/add-cart', [ AddCartController::class, 'add' ])->name('add-cart');
Route::get('cart', [ CartController::class, 'list' ])->name('cart');
Route::get('remove-item/{id}', [ RemoveItem::class ,'delete' ])->name('remove-item');

// for checkout
Route::post('/checkout', [ CheckoutController::class, 'checkout' ])->name('checkout');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

// for product
Route::view('/add-product', 'custom.add-product')->middleware(['auth', 'can:admin_permission'])->name('add-product');
Route::post('/add-product', [ AddProductController::class, 'add' ])->middleware(['auth'])->name('add-product');

// for orders
Route::get('/orders', [OrderModelController::class, 'list'])->middleware(['auth'])->name('orders');

require __DIR__.'/auth.php';
