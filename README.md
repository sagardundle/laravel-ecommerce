# E Commerce website using Lravel 8.18

## Steps to Install
    * Composer Install
    * Import database from db folder
    * npm install and npm run dev

## Home page
![picture](img/home.PNG)
![picture](img/home1.PNG)

## Games Page
![picture](img/games.PNG)

## Movies Page
![picture](img/movies.PNG)

## Electronics Page
![picture](img/electronics.PNG)

## cart Page
![picture](img/cart.PNG)
![picture](img/cart1.PNG)

## Registration page
![picture](img/registration.PNG)

## Login page
![picture](img/login.PNG)

## Admin Dashboard page
![picture](img/admin-dashboard.PNG)

## Add PRoduct page
![picture](img/add-product.PNG)
![picture](img/add-product1.PNG)

## Admin Orders
![picture](img/admin-orders.PNG)

## User Orders
![picture](img/user-orders.PNG)

## Logged in Dashboard homepage
![picture](img/logged-in-home.PNG)
