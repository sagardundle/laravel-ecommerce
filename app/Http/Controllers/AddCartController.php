<?php

namespace App\Http\Controllers;

use App\Models\AddCart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddCartController extends Controller
{
    public function add(Request $request)
    {
        $fields = $request->validate([
            'product_id' => 'required|integer'
        ]);

        // check if user is logged in or not
        if(Auth::check() == false){
            // add the product id to cookie
            if(!empty($_COOKIE['product_id'])){
                $product_ids = [$_COOKIE['product_id']];
                // dd(array_search($request->product_id, $product_ids));
                array_push( $product_ids , $request->product_id);
                setcookie('product_ids', json_encode($product_ids), time() + 60*100000, '/');
                setcookie('product_id', '', 1, '/');
            }elseif(!empty($_COOKIE['product_ids'])){
                $product_ids = json_decode($_COOKIE['product_ids'], true);
                array_push( $product_ids , $request->product_id);
                setcookie('product_ids', json_encode($product_ids), time() + 60*100000, '/');
            }else{            
                setcookie('product_id', $request->product_id, time() + 60*100000, '/');
            }
        }elseif(Auth::check() == true){
            // $add_cart = AddCart::create($fields);
            $add_cart = new AddCart();
            $add_cart->uid = Auth::id();
            $add_cart->product_id = $request->product_id;
            $add_cart->save();
        }

        // $add_cart = AddCart::create($fields);
        // if($add_cart){
            $request->session()->flash('success', 'Product added successfully');
            return redirect()->route('cart');
        // }
    }
}
