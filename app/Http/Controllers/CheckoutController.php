<?php

namespace App\Http\Controllers;

use App\Models\AddCart;
use App\Models\Checkout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
    public function checkout(Request $request)
    {   
        $id = Auth::id();
        if(empty($id)){
            $request->session()->flash('info', 'Please Register/Login first');
            session(['previous_page' => $request->path()]); // set the url if user has visited cart page and redirect to him after registration/login
            return redirect()->route('register');
        }elseif(!empty($id)){
            // $item = $request->validate([
            //     'product_id[]' => 'required|integer',
            // ]);
            
            $total_products = count($request->product_id);
            for($i=0; $i<$total_products ;$i++){
                $checkout = new Checkout();
                $checkout->product_id = $request->product_id[$i];
                $checkout->user_id = $id;
                $checkout->save();

                // reset cookie once order is completed
                if(!empty($_COOKIE['product_id']) || !empty($_COOKIE['product_ids'])){
                    setcookie('product_id', '', 1, '/');
                    setcookie('product_ids', '', 1, '/');
                }
                // $delete_cart_items = AddCart::find($request->product_id[$i])->delete();
            }
            
            $request->session()->flash('order-added', 'Order Placed Successfully');
            return redirect()->route('home');
        }
    }
}
