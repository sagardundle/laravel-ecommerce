<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function list()
    {
        // check if user is logged in or not
        if(Auth::check() == false){
            // dd(json_decode($_COOKIE['product_ids']));
            if(!empty($_COOKIE['product_ids'])){
                $ids = json_decode($_COOKIE['product_ids'], true);
                $cart = [];
                foreach($ids as $id){
                    $cart[] = DB::table('products as p')
                                ->where('p.id', $id)
                                ->select('p.*')->get();
                }

            }elseif(!empty($_COOKIE['product_id'])){
                $cart[] = DB::table('products as p')
                            ->select('p.*')->where('p.id', $_COOKIE['product_id'])->get();
            }else{
                $cart = [];
            }
        }elseif(Auth::check() == true){

            $cart[] = DB::table('products as p')
                        ->rightJoin('cart as c', 'p.id', '=', 'c.product_id')
                        // ->where('p.id', $id)
                        ->where('c.uid', Auth::id())
                        ->select('p.*', 'c.*')->get();
                        
        }
        $total_items = count($cart[0]->all());
        // store count in session
        session(['total_items_in_cart' => $total_items]);

        return view('custom.cart', [ 'cart' => $cart, 'total_items' => $total_items ]);
    }
}
