<?php

namespace App\Http\Controllers;

use App\Models\GetProduct;
use Illuminate\Http\Request;

class ElectronicsController extends Controller
{
    public function list()
    {
        $electronics = GetProduct::where('category', 'electronics')->get();
        return view('frontend.electronics', [ 'electronics' => $electronics ]);
    }
}
