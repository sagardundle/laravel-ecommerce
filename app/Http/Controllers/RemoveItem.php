<?php

namespace App\Http\Controllers;

use App\Models\AddCart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RemoveItem extends Controller
{
    public function delete($id)
    {
        // check if user is logged in or not
        if(Auth::check() == false){
            if(!empty($_COOKIE['product_ids'])){
                $ids = json_decode($_COOKIE['product_ids'], true);
                if (($key = array_search($id, $ids)) !== false) {
                    unset($ids[$key]);
                }
            }elseif(!empty($_COOKIE['product_id'])){
                $ids = [];
                setcookie('product_ids', '', 1, '/');
                setcookie('product_id', '', 1, '/');
            }
        
            if(count($ids) > 1){
                setcookie('product_ids', json_encode($ids), time() + 60*100000, '/');
            }elseif(count($ids) == 1){
                $value = current($ids);
                setcookie('product_ids', '', 1, '/');
                setcookie('product_id', $value, time() + 60*100000, '/');
            }
        }elseif(Auth::check() == true){
            AddCart::find($id)->delete();
        }
        
        return redirect()->route('cart');
    }
}
