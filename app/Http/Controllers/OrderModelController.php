<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\OrderModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class OrderModelController extends Controller
{
    public function list()
    {
        $user = User::find(Auth::id());

        if($user->roles == '2'){ // for users display their orders
            $orders = OrderModel::join('products', 'orders.product_id', '=', 'products.id')
                                    ->join('users', 'orders.user_id', '=', 'users.id')
                                    ->where('orders.user_id', Auth::id())->get();
        }elseif($user->roles == '1'){ // for admin display all orders
            $orders = OrderModel::join('products', 'orders.product_id', '=', 'products.id')
                                    ->join('users', 'orders.user_id', '=', 'users.id')->get();
        }

        return view('custom.order-list', [ 'orders' => $orders ]);
    }
}
