<?php

namespace App\Http\Controllers;

use App\Models\GetProduct;
use Illuminate\Http\Request;

class GetProductController extends Controller
{
    public function get()
    {
        $products = GetProduct::all();
        return view('home', [ 'products' => $products ]);
    }
}
