<?php

namespace App\Http\Controllers;

use App\Models\GetProduct;
use Illuminate\Http\Request;

class GamesController extends Controller
{
    public function list()
    {
        $games = GetProduct::where('category', 'games')->get();
        return view('frontend.games', [ 'games' => $games ]);
    }
}
