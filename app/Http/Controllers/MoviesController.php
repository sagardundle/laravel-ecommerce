<?php

namespace App\Http\Controllers;

use App\Models\GetProduct;
use Illuminate\Http\Request;

class MoviesController extends Controller
{
    public function list()
    {
        $movies = GetProduct::where('category', 'movies')->get();
        return view('frontend.movies', [ 'movies' => $movies ]);
    }
}
