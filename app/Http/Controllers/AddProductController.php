<?php

namespace App\Http\Controllers;

use App\Models\AddProduct;
use Illuminate\Http\Request;

class AddProductController extends Controller
{
    public function add(Request $request)
    {
        // dd(public_path('\images'));
        $fields = $request->validate([
            'title' => 'required|min:3|max:255',
            'category' => 'required',
            'description' => 'required|min:3',
            'photo' => 'required|image|mimes:jpeg,jpg,png,svg,jfif',
            'price' => 'required',
        ],);

        $file = $request->file('photo');
        $file->move(public_path('\images'), $file->getClientOriginalName()); 
        
        $product = new AddProduct();
        $product->title = $request->title;
        $product->category = $request->category;
        $product->description = $request->description;
        $product->photo = $file->getClientOriginalName();
        $product->price = $request->price;


        // $save = AddProduct::create($fields);
        if($product->save()){
            $request->session()->flash('status', 'Product added successfully');
            return redirect()->route('add-product');
        }
    }
}
